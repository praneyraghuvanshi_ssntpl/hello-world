<?php
	require_once("connection.php");
?>
<html>
<head>
<title>Database Entries</title>
</head>

<body>
<h3>Entries from database</h3>

<table border="1">
<tr><th>Name</th></tr>

	<?php
		//query for fetching data from database
		$sql = "SELECT name from user";
		$result = mysqli_query($conn,$sql);
		
		if(mysqli_num_rows($result)>0)
		{
			while($row = mysqli_fetch_assoc($result))
			{
				echo "<tr><td>".$row['name']."</td></tr>";
			}
		}
		else
		{
			echo "Error in fetching data.";
		}
		mysqli_close($conn);
	?>
	</table>
	
</body>

</html>