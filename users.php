<?php 

//Connect to database
$conn = mysqli_connect('localhost','root','','demo');

$request_method = $_SERVER['REQUEST_METHOD'];
switch($request_method)
{
	case 'GET':
		if(!empty($_GET['user_id']))
		{
			$user_id = intval($_GET['user_id']);
			get_users($user_id);
		}
		else
		{
			get_users(); // User::get_users();
		}
		break;

	case 'POST':
		if (!empty($_GET['user_id'])) {
			$user_id = intval($_GET['user_id']);
			update_user($user_id);
		}
		else 
		{
			insert_user();	
		}
		break;

	case 'DELETE':
			$user_id = intval($_GET['user_id']);
			delete_user($user_id);
		break;
		default:
		//Invalid Request Method
		header("HTTP/1.0 405 Method Not Allowed");
		break;			
}

	function get_users($user_id=0)
	{
		global $conn;
		$query = "SELECT * FROM user";
		if($user_id != 0)
		{
			$query.=" WHERE id=".$user_id." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn,$query);
		while($row=mysqli_fetch_array($result))
		{
			$response[] =array(
				'id' => $row['id'],
				'name' => $row['name']
			);
		}
		if($response==NULL)
		{
			$response[] = array('Error ' => 'Requested ID is not in database.');
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	function insert_user()
	{
		global $conn;
		$name = $_POST['name'];
		$response = array('status' => 0,'status_message' =>'Name addition failed!');
		if(!empty($_POST['name'])) 
		{
		$query = "INSERT INTO user(id,name) VALUES('','$name')";
		if(mysqli_query($conn,$query))
		{
			$response = array(
				'status' => 1,'status_message' =>'Name added successfully.'
			);
		}
		else
		{
			$response = array(
				'status' => 0,'status_message' =>'Name addition failed!'
			);
		}
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	function update_user($user_id)
	{
		global $conn;
		$name = $_POST['name'];
		$response = array('status'=>0,'status_message'=>'Name updation failed!');
		if (!empty($_POST['name'])) {
			$query = "UPDATE user SET name = '$name' WHERE id = '$user_id' ";
			if(mysqli_query($conn,$query))
			{
				$response = array('status'=>1,'status_message'=>'Name updated successfully.');
			}
			else
			{
				$response = array('status'=>0,'status_message'=>'Name updation failed!');
			}
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	function delete_user($user_id)
	{
		$response = array('status'=>0,'status_message'=>'User deletion failed');
		if($user_id!=NULL)
		{
			global $conn;
		$query = "DELETE FROM user WHERE id =".$user_id;
		if (mysqli_query($conn,$query))
		{
			$response = array('status'=>1,'status_message'=>'User deleted successfully');
		}
		else
		{
			$response = array('status'=>0,'status_message'=>'User deletion failed');
		}
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

mysqli_close($conn);
?>